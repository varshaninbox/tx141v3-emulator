# TX141v3 Emulator #

An emulator of the TX141v3 temperature only sensor for weather station clocks using a Raspberry Pi and OpenWeatherMap. The sensor uses OOK modulation to send out 6 repeated packets every 30 seconds.

### Installation ###

* Clone this repository on your pi.
* Install dependencies `libcurl` and `gpiod` using `sudo apt install libcurl4-openssl-dev` and `sudo apt install libgpiod-dev`
* Edit `settings.txt` to put in your API key from OpenWeatherMap and location coordinates.
* Connect a cheap 433MHz OOK transmitter to pin 0 (or other pin) and use 3.3v to power it.
* Create the build directory using `mkdir build && cd build`.
* Set CMake to build `cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release`.
* Use CMake to build `make`.
* Move or copy the build to a folder with `settings.txt`.
* Optionally create a `systemctl` service to run the program at boot time. You can edit and load `tx141v3.service` as a system service to run at boot time. Move the file to `/etc/systemd/system/` and then run `sudo systemctl enable tx141v3.service`.