/*
    TX141V3 Emulator
    Varshan S.

    TX141V3 is a weather station sensor that uses OOK modulation to transmit 
    temperature data.

    Each packet is 32 bits long and repeated 6 times.

    - 8 bits for id
    - 4 flag bits. 1st bit is battery indicator.
    - 12 bits for temperature. (Float format xx.x * 10, truncated to int12.)
    - 8 bit checksum LFSR-based Toeplitz hash


    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
*/
#include "TX141.hpp"
#include <stdint.h>
#include <cmath>
#include <iostream>
#include <string>
#include <gpiod.h>

// Burst Timing Constants
#define HIGH_TX_DURATION 775
#define ONE_TX_DURATION 3650
#define ZERO_TX_DURATION 1600
#define PAUSE_TX_DURATION 8500

TX141::TX141(int tx_pin, int sensor_id) : txPin(tx_pin), sensorId(sensor_id) {
    ctx = gpiod_chip_open_by_name("gpiochip0");
    if (!ctx) {
        throw std::runtime_error("Failed to open GPIO chip.");
    }

    line = gpiod_chip_get_line(ctx, tx_pin);
    if (!line) {
        throw std::runtime_error("Failed to get GPIO line.");
    }

    if (gpiod_line_request_output(line, "TX141", 0) < 0) {
        throw std::runtime_error("Failed to request GPIO output.");
    }
}

TX141::~TX141() {
    gpiod_line_release(line);
    gpiod_chip_close(ctx);
}

uint8_t TX141::calculateChecksum(const uint8_t message[], unsigned bytes, uint8_t keystream, uint8_t key) {
    uint8_t checksum = 0;

    for (unsigned k = 0; k < bytes; ++k) {
        uint8_t data = message[k];
        for (int i = 7; i >= 0; --i) {
           if ((data >> i) & 1)
                checksum ^= key;
            if (key & 1)
                key = (key >> 1) ^ keystream;
            else
                key = (key >> 1);
        }
    }
    return checksum;
}

void TX141::sendBit(bool bit) {
    gpiod_line_set_value(line, 1);
    usleep(HIGH_TX_DURATION);
    gpiod_line_set_value(line, 0);
    usleep(bit ? ONE_TX_DURATION : ZERO_TX_DURATION);
}

void TX141::sendPacket(const uint8_t *packet) {
    for (int i = 0; i < BYTE_LENGTH; i++) {
        uint8_t byte = *packet++;
        for (int j = 7; j >= 0; j--) {
            sendBit((byte >> j) & 1);
        }
    }
}

void TX141::sendRepeatedPackets(const uint8_t *packet, int repeatCount) {
    for (int i = 0; i < repeatCount; i++) {
        gpiod_line_set_value(line, 1);
        usleep(HIGH_TX_DURATION);
        gpiod_line_set_value(line, 0);
        usleep(PAUSE_TX_DURATION);
        sendPacket(packet);
        gpiod_line_set_value(line, 1);
        usleep(HIGH_TX_DURATION);
        gpiod_line_set_value(line, 0);
    }
}

void TX141::setPacket(float temperature) {
    int16_t temp_int16 = (int16_t) round(temperature * 10);
    uint8_t sensorIdLowByte = static_cast<uint8_t>(sensorId & 0xFF);
    packet[0] = sensorIdLowByte;
    packet[1] = 0x80 | (uint8_t) ((temp_int16 & 0x0F00) >> 8);
    packet[2] = (uint8_t) (temp_int16 & 0x00FF);
    packet[3] = calculateChecksum(packet, 3, 0x98, 0xf1);
}

bool TX141::sendTemperaturePackets(float temperature) {
    setPacket(temperature);
    sendRepeatedPackets(packet, REPEAT_COUNT);
    return true; // For future error handling if needed
}