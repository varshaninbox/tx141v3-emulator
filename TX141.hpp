#ifndef TX141_HPP
#define TX141_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <unistd.h>
#include <cstdio>

class TX141 {
public:
    // Constructor and Destructor
    TX141(int tx_pin, int sensor_id);
    ~TX141();

    // Constants
    static constexpr int BYTE_LENGTH = 4;
    static constexpr int REPEAT_COUNT = 6;
    static constexpr int HIGH_TX_DURATION = 775;
    static constexpr int ONE_TX_DURATION = 3650;
    static constexpr int ZERO_TX_DURATION = 1600;
    static constexpr int PAUSE_TX_DURATION = 8500;

    // Methods
    bool sendTemperaturePackets(float temperature);

private:
    // Member Variables
    int txPin;
    int sensorId;
    struct gpiod_chip *ctx;
    struct gpiod_line *line;
    uint8_t packet[BYTE_LENGTH];

    // Private methods
    void exportPin(int pin);
    void unexportPin(int pin);
    void setDirection(int pin, const std::string& direction);
    void writeValue(int pin, int value);
    std::string formatString(const std::string& format, int pin);
    uint8_t calculateChecksum(const uint8_t message[], unsigned bytes, uint8_t keystream, uint8_t key);
    void sendBit(bool bit);
    void sendPacket(const uint8_t *packet);
    void sendRepeatedPackets(const uint8_t *packet, int repeatCount);
    void setPacket(float temperature);
};

#endif