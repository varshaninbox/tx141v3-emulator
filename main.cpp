#include "TX141.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <curl/curl.h>
#include <thread>
#include <mutex>
#include <ctime>
#include <cstring>

// Global variables
float temp = 0.0;
bool has_temp = false;
unsigned long tx_141_time = 0;
std::mutex temp_mutex;

// Structure to hold memory data for curl callback
struct memory_struct {
    char *response;
    size_t size;
};

// Function to read settings from text file
void read_settings(const std::string& filename, std::string& api_key, std::string& lat, std::string& lon, int& sensor_pin, int& sensor_id) {
    std::ifstream settings_file(filename);
    if (settings_file.is_open()) {
        std::string line;
        while (std::getline(settings_file, line)) {
            size_t pos = line.find("=");
            if (pos != std::string::npos) {
                std::string key = line.substr(0, pos);
                std::string value = line.substr(pos + 1);
                if (key == "API_KEY")
                    api_key = value;
                else if (key == "lat")
                    lat = value;
                else if (key == "lon")
                    lon = value;
                else if (key == "SENSOR_PIN")
                    sensor_pin = std::stoi(value);
                else if (key == "SENSOR_ID")
                    sensor_id = std::stoi(value);
            }
        }
        settings_file.close();
    } else {
        std::cerr << "Error: Unable to open settings file." << std::endl;
        std::exit(1);
    }
}

// Callback function to write data received by curl
static size_t memory_callback(void *data, size_t size, size_t nmemb, void *userp) {
    size_t real_size = size * nmemb;
    struct memory_struct *mem = (struct memory_struct *)userp;

    char *ptr = (char*) realloc(mem->response, mem->size + real_size + 1);
    if(ptr == NULL)
        return 0;

    mem->response = ptr;
    memcpy(&(mem->response[mem->size]), data, real_size);
    mem->size += real_size;
    mem->response[mem->size] = 0;

    return real_size;
}

// Function to fetch JSON data from OpenWeatherMap API
void get_json(const std::string& lat, const std::string& lon, const std::string& API_KEY) {
    CURL *curl;
    CURLcode res;
    struct memory_struct chunk = {0};
    
    curl = curl_easy_init();
    if (curl) {
        // Specify URL to get
        std::string openWeatherURL = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=" + API_KEY + "&units=metric";
        curl_easy_setopt(curl, CURLOPT_URL, openWeatherURL.c_str());
     
        // Send all data to memory_callback function
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, memory_callback);
        
        // Pass 'chunk' struct to the callback function
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
        
        // Perform the request, res will get the return code
        res = curl_easy_perform(curl);

        if (res == CURLE_OK) {
            try {
                std::string response(chunk.response);
                size_t pos = response.find("temp\":");
                if (pos != std::string::npos) {
                    size_t end_pos = response.find(",", pos);
                    if (end_pos != std::string::npos) {
                        temp_mutex.lock();
                        temp = std::stof(response.substr(pos + 6, end_pos - pos - 6));
                        has_temp = true;
                        temp_mutex.unlock();
                    }
                }
            } catch (std::exception& e) {
                std::cerr << "Error: " << e.what() << std::endl;
            }
        } else {
            std::cerr << "Error: " << curl_easy_strerror(res) << std::endl;
        }

        // Cleanup
        curl_easy_cleanup(curl);
    }
}

// Function to continuously fetch JSON data
void json_update(const std::string& lat, const std::string& lon, const std::string& API_KEY) {
    while(true) {
        get_json(lat, lon, API_KEY);
        std::cout << "Temperature: " << temp << std::endl;
        std::this_thread::sleep_for(std::chrono::minutes(10));
    }
}

// Function to handle TX141 transmission
void tx_141(int sensor_pin, int sensor_id, const std::string& lat, const std::string& lon, const std::string& API_KEY) {
    TX141 sensor(sensor_pin, sensor_id);

    unsigned long tx_141_time = (unsigned long) std::time(0);
    unsigned long tx_141_time_now = tx_141_time;

    while (!has_temp) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    while (true) {
        if ((tx_141_time_now - tx_141_time) % 30 != 0) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        } else {
            tx_141_time = tx_141_time_now;
            temp_mutex.lock();
            sensor.sendTemperaturePackets(temp);
            
            std::cout << "Sent 433MHz." << std::endl;

            temp_mutex.unlock();
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        tx_141_time_now = (unsigned long) std::time(0);
    }
}

int main() {
    std::string api_key, lat, lon;
    int sensor_pin;
    int sensor_id;

    read_settings("settings.txt", api_key, lat, lon, sensor_pin, sensor_id);

    std::cout << "Latitude: " << lat << std::endl;
    std::cout << "Longitude: " << lon << std::endl;
    std::cout << "Sensor Pin: " << sensor_pin << std::endl;
    std::cout << "Sensor ID: " << sensor_id << std::endl;

    std::thread t1(tx_141, sensor_pin, sensor_id, lat, lon, api_key);
    std::thread t2(json_update, lat, lon, api_key);
    t1.join();
    t2.join();
    
    return 0;
}